package worker

import (
	"fmt"
	"log"
	"reflect"
	"time"
)

type jobDuration struct {
	jobType  reflect.Type
	duration time.Duration
}

type diagData struct {
	minimum   time.Duration
	maximum   time.Duration
	totalTime time.Duration
	count     int
}

func (dd diagData) String() string {
	return fmt.Sprintf("Count: %v Avg: %v Minimum: %v Maximum: %v Total: %v",
		dd.count, dd.totalTime/time.Duration(dd.count), dd.minimum, dd.maximum, dd.totalTime)
}

type DiagOpts struct {
	CongestionLogLimit  int
	DurationLogInterval int
	Logger              Logger
}

type DiagHandler struct {
	name               string
	logger             Logger
	opts               DiagOpts
	killChan           chan struct{}
	lastestQueuedJob   int
	queuedJobChan      chan int
	latestCompletedJob int
	completedJobChan   chan int
	diagCount          int
	durationChan       chan jobDuration
	diagMap            map[reflect.Type]*diagData
}

func NewDiagHandler(name string, opts DiagOpts) *DiagHandler {
	dh := &DiagHandler{
		name:     name,
		opts:     opts,
		killChan: make(chan struct{}),
		diagMap:  make(map[reflect.Type]*diagData),
	}

	if dh.opts.CongestionLogLimit > 0 {
		dh.queuedJobChan = make(chan int)
		dh.completedJobChan = make(chan int)
	}

	if dh.opts.DurationLogInterval > 0 {
		dh.durationChan = make(chan jobDuration)
	}

	go dh.run()

	return dh
}

func (dh *DiagHandler) log(format string, v ...interface{}) {
	if dh.opts.Logger != nil {
		dh.opts.Logger.Printf(fmt.Sprintf("[%v] ", dh.name)+format, v...)
	} else {
		log.Printf(fmt.Sprintf("[%v] ", dh.name)+format, v...)
	}
}

func (dh *DiagHandler) printDiag() {
	dh.log("Diag:")
	for jobType, diagData := range dh.diagMap {
		dh.log("  %v: %v", jobType, diagData)
	}
}

func (dh *DiagHandler) printCongestion() {
	diff := dh.lastestQueuedJob - dh.latestCompletedJob
	if diff > dh.opts.CongestionLogLimit {
		dh.log("Job congestion: %v", diff)
	}
}

func (dh *DiagHandler) run() {
	for {
		select {
		case diagDur := <-dh.durationChan:
			var data *diagData
			var ok bool
			if data, ok = dh.diagMap[diagDur.jobType]; !ok {
				data = &diagData{}
				dh.diagMap[diagDur.jobType] = data
			}

			if data.minimum == 0 || diagDur.duration < data.minimum {
				data.minimum = diagDur.duration
			}

			if diagDur.duration > data.maximum {
				data.maximum = diagDur.duration
			}

			data.totalTime += diagDur.duration
			data.count++
			dh.diagCount++

			if dh.diagCount%dh.opts.DurationLogInterval == 0 {
				dh.printDiag()
				for k := range dh.diagMap {
					delete(dh.diagMap, k)
				}
			}
		case dh.lastestQueuedJob = <-dh.queuedJobChan:
			dh.printCongestion()
		case dh.latestCompletedJob = <-dh.completedJobChan:
			dh.printCongestion()
		case <-dh.killChan:
			return
		}
	}
}

func (dh *DiagHandler) logJob(jobType reflect.Type, duration time.Duration) {
	if dh.durationChan == nil {
		return
	}

	select {
	case dh.durationChan <- jobDuration{
		jobType:  jobType,
		duration: duration,
	}:
	case <-dh.killChan:
	}
}

func (dh *DiagHandler) logStart(workerID int) {
	if dh.queuedJobChan == nil {
		return
	}

	select {
	case dh.queuedJobChan <- workerID:
	case <-dh.killChan:
	}
}

func (dh *DiagHandler) logCompletion(workerID int) {
	if dh.completedJobChan == nil {
		return
	}

	select {
	case dh.completedJobChan <- workerID:
	case <-dh.killChan:
	}
}

func (dh *DiagHandler) kill() {
	close(dh.killChan)
}
