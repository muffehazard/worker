package worker

import (
	"fmt"
	"reflect"
	"time"
)

type Job interface {
	Do() error
}

const (
	None  = -1
	Err   = 0
	Info  = 1
	Debug = 2
)

type RetVal struct {
	WorkerId int
	Err      error
}

type WorkerOpts struct {
	Logger         Logger
	RetChan        chan RetVal
	NotifyInterval time.Duration
	PrintLevel     int
	DiagHandler    *DiagHandler
}

type Worker struct {
	workerId       int
	prevWorker     *Worker
	logger         Logger
	retChan        chan RetVal
	stopChan       chan struct{}
	killChan       chan struct{}
	job            Job
	printLevel     int
	notifyInterval time.Duration
	diagHandler    *DiagHandler
}

func (w Worker) String() string {
	return fmt.Sprintf("Worker %v: %v", w.workerId, reflect.TypeOf(w.job))
}

func NewWorker(opts *WorkerOpts) *Worker {
	nw := &Worker{
		workerId: 0,
		stopChan: make(chan struct{}),
		killChan: make(chan struct{}),
	}

	if opts != nil {
		nw.logger = opts.Logger
		nw.retChan = opts.RetChan
		nw.printLevel = opts.PrintLevel
		nw.diagHandler = opts.DiagHandler
		nw.notifyInterval = opts.NotifyInterval
	}

	nw.logMsg(Debug, "New worker: %+v", nw)
	close(nw.stopChan)

	return nw
}

func (w *Worker) logMsg(level int, msg string, v ...interface{}) {
	if w.printLevel >= level && w.logger != nil {
		w.logger.Printf(fmt.Sprintf("[%s] ", w)+msg, v...)
	}
}

func (w *Worker) sendError(err error) {
	if err != nil {
		w.logMsg(Err, "Error: %v", err)
	}

	if w.retChan != nil {
		select {
		case w.retChan <- RetVal{
			WorkerId: w.workerId,
			Err:      err,
		}:
		case <-w.killChan:
		}
	}
}

func (w *Worker) handle() {
	w.logMsg(Info, "Handling")
	defer func() {
		w.prevWorker = nil
		if w.diagHandler != nil {
			w.diagHandler.logCompletion(w.workerId)
		}
		w.logMsg(Info, "Handling done")
	}()

	select {
	case <-w.killChan:
		w.logMsg(Debug, "Killed")
		close(w.prevWorker.killChan)
		return
	case <-w.prevWorker.stopChan:
		w.logMsg(Debug, "Previous job done")
	}

	w.prevWorker = nil
	if w.notifyInterval > 0 {
		go w.time()
	}

	w.logMsg(Info, "Starting job")
	startTime := time.Now()
	w.sendError(w.job.Do())
	duration := time.Now().Sub(startTime)
	w.logMsg(Info, "Done job: %v", duration)

	if w.diagHandler != nil {
		w.diagHandler.logJob(reflect.TypeOf(w.job), duration)
	}

	close(w.stopChan)
}

func (w *Worker) time() {
	w.logMsg(Info, "Starting notify ticker")
	ticker := time.NewTicker(w.notifyInterval)
	defer ticker.Stop()

	n := 0

	select {
	case <-ticker.C:
		n++
		w.logMsg(Err, "Notify: runtime %v", time.Duration(n)*w.notifyInterval)
	case <-w.stopChan:
		w.logMsg(Info, "Notify: completed after %v intervals", n)
	case <-w.killChan:
		w.logMsg(Info, "Notify: killed after %v intervals", n)
	}
}

func (w *Worker) NextWorker(job Job) (int, *Worker) {
	nw := &Worker{
		workerId:       w.workerId + 1,
		prevWorker:     w,
		logger:         w.logger,
		retChan:        w.retChan,
		stopChan:       make(chan struct{}),
		killChan:       make(chan struct{}),
		job:            job,
		notifyInterval: w.notifyInterval,
		printLevel:     w.printLevel,
		diagHandler:    w.diagHandler,
	}

	go nw.handle()

	if nw.diagHandler != nil {
		nw.diagHandler.logStart(nw.workerId)
	}

	return nw.workerId, nw
}

func (w *Worker) Kill() {
	if w.diagHandler != nil {
		w.diagHandler.kill()
	}

	close(w.killChan)
}
